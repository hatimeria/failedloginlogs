<?php
/**
 * Login logs observer class
 *
 * @category   Hatimeria
 * @package    Hatimeria_LoginLogs
 * @author     Piotr Matras <piotr.matras@hatimeria.pl>
 */
class Hatimeria_LoginLogs_Model_Observer
{
    /**
     * File name for logs
     */
    const ADMIN_LOGFILE = 'loginlogs_admin.log';

    /**
     * Log admin failed login
     *
     * @event admin_session_user_login_failed
     * @param Varien_Event_Observer $observer
     */
    public function logAdminFailedLogin(Varien_Event_Observer $observer)
    {
        $ipAddress = Mage::helper('core/http')->getRemoteAddr();
        Mage::log($ipAddress, null, self::ADMIN_LOGFILE);
    }
}